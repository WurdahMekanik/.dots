export ARCHDROP=$HOME/Dropbox/SHARE/ARCH
export HOMEDROP=$ARCHDROP/home
export THEMEDROP=$ARCHDROP/usr/share/themes
export ROOTDROP=$ARCHDROP/root

export MIKRO=$HOME/Dropbox/SHARE/_BEND/_MIKRO
export PROJECTS=$MIKRO/_projects
export DATASHEETS=$HOME/Dropbox/SHARE/_BEND/_DATA
export SCHEM=$HOME/Dropbox/SHARE/_BEND/_SCHEM

export PIKDIR=$MIKRO/_projects/PIC
export PYCLASS=$HOME/Dropbox/SHARE/_EDU/_ee/Python
export NETCLASS=$HOME/Dropbox/SHARE/_EDU/_ee/Networking

export EDITOR=vim

export ZDOTDIR=$HOME/.zsh
