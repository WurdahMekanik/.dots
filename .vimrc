set term=ansi
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab
set smarttab
syntax on
autocmd! BufNewFile,BufRead *.pde setlocal ft=arduino
autocmd! BufNewFile,BufRead *.css.lens setlocal ft=css
autocmd! BufNewFile,BufRead *.lhs setlocal ft=lhaskell

